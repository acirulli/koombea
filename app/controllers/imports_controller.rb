class ImportsController < ApplicationController
  before_action :set_import, only: %i[ show edit update destroy ]
  before_action :authenticate_user!


  # GET /imports or /imports.json
  def index
    @imports = Import.where(user_id: current_user.id)
    @contacts = Contact.by_user_id(current_user.id)
  end

  # GET /imports/1 or /imports/1.json
  def show
    @contacts = @import.contacts
    @contact_failures = @import.contact_failures
  end

  # GET /imports/new
  def new
    @import = Import.new
  end

  # POST /imports or /imports.json
  def create
    file = params[:import][:file]


    @import = Import.new({user_id: current_user.id, status: :on_hold})

    respond_to do |format|
      if @import.save
        @import.process_csv(file)
        format.html { redirect_to @import, notice: "Import #{@import.status}" }
        format.json { render :show, status: :created, location: @import }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @import.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /imports/1 or /imports/1.json
  def update
    respond_to do |format|
      if @import.update(import_params)
        format.html { redirect_to @import, notice: "Import was successfully updated." }
        format.json { render :show, status: :ok, location: @import }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @import.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /imports/1 or /imports/1.json
  def destroy
    @import.destroy
    respond_to do |format|
      format.html { redirect_to imports_url, notice: "Import was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_import
      @import = Import.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def import_params
      params.fetch(:import, {})
    end


end
