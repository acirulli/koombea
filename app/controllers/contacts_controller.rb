class ContactsController < ApplicationController
  before_action :set_contact, only: %i[ show edit update destroy ]
  before_action :authenticate_user!
  before_action :get_import, only: [:index]

  def all
    @contacts = Contact.by_user_id(current_user.id)
    render :index
  end

  def index
    @contacts = @import.contacts
  end

  private

  def get_import
    @import = Import.where(user_id: current_user.id).where(id: params[:import_id]).first
    not_found unless @import.nil?
  end

end
