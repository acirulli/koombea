class ApplicationController < ActionController::Base

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  protected
  def authenticate_user!
    if user_signed_in?
      super
    else
      redirect_to new_user_session_path, :notice => 'Please login'
    end
  end
end
