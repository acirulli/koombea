class Contact < ApplicationRecord
  require 'credit_card_validations/string'
  belongs_to :import, inverse_of: :contacts
  belongs_to :user, inverse_of: :contacts
  scope :by_user_id, -> (user_id) {where('user_id = ?', user_id)}
  validates_presence_of :credit_card_last_4_digits, :name, :birth_date, :phone, :address, :credit_card, :franchise, :email
  validates :credit_card, presence: true, credit_card_number: true
  validate :validate_phone
  validates :email, uniqueness: { scope: [:user_id] }
  validates :email, format: { with: Devise.email_regexp }


  after_validation do
    write_attribute(:credit_card, Digest::SHA1.hexdigest(credit_card)) if credit_card.present?
  end

  def credit_card=(val)
    write_attribute(:franchise, val&.credit_card_brand_name)
    write_attribute(:credit_card_last_4_digits, val&.last(4).to_s)
    write_attribute(:credit_card, val)
  end

  def validate_phone
    regex_format1 = '\(\+\d{2}\)\s\d{3}\s\d{3}\s\d{2}\s\d{2}\s\d{2}'
    regex_format2 = '\(\+\d{2}\)\s\d{3}-\d{3}-\d{2}-\d{2}-\d{2}'
    if self.phone.present? and !self.phone.match(regex_format1) and !self.phone.match(regex_format2)
      errors.add(:phone, "invalid format")
    end
  end

end
