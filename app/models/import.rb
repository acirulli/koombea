require 'csv'
class Import < ApplicationRecord
  attr_accessor :csv_positions
  has_many :contacts, inverse_of: :import
  has_many :contact_failures, inverse_of: :import
  belongs_to :user, inverse_of: :imports
  enum status: [:on_hold, :processing, :failed, :finished]


  def csv_positions
    return @csv_positions if @csv_positions
    return {
      name: 0,
      phone: 1,
      birth_date: 2,
      address: 3,
      credit_card: 4,
      email: 5
    }
  end


  def process_csv(file)
    self.update_attribute(:status, :processing)
    can_have_status_finished = false
    begin
      csv_text = File.read(file)
      csv = CSV.parse(csv_text, :encoding => 'ISO-8859-1')
      csv.each_with_index do |row, index|
        contact = Contact.new({
          name: row[csv_positions[:name]],
          phone: row[csv_positions[:phone]],
          birth_date: row[csv_positions[:birth_date]],
          address: row[csv_positions[:address]],
          credit_card: row[csv_positions[:credit_card]],
          email: row[csv_positions[:email]],
          import_id: self.id,
          user_id: self.user.id
        })
        if contact.save
          can_have_status_finished = true
        else
          ContactFailure.create!({
            import_id: self.id,
            row: index,
            log: contact.errors&.messages&.to_json
          })
        end
      end
    rescue Exception => e
      Rails.logger.error("Exception found in processing csv: #{e.message}")
    ensure
      self.update_attribute(:status, can_have_status_finished ? :finished  : :failed)
    end
  end

end
