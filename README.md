# README

## Environment Setup

- Install Docker on your machine
- run `docker-compose build`
- run `docker-compose run web rails db:create db:migrate`
- run `docker-compose up`
- Connect to [http://localhost:3000](http://localhost:3000)


## CSV Example
Csv example _contacts.csv_ can be found in the root dir