Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: "imports#index"
  resources :imports do
    get 'contacts', to: 'contacts#index'
  end

  get 'contacts', to: 'contacts#all'
end
