class ChangeStatusTypeImports < ActiveRecord::Migration[6.0]
  def up
    Import.all.each do |i|
      i.update_attributes({status: nil})
    end
    change_column :imports, :status, :integer
  end

  def down
    change_column :imports, :status, :string
  end
end
