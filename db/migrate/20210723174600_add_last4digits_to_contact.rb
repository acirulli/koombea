class AddLast4digitsToContact < ActiveRecord::Migration[6.0]
  def change
    add_column :contacts, :credit_card_last_4_digits, :string
  end
end
