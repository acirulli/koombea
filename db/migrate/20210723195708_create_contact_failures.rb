class CreateContactFailures < ActiveRecord::Migration[6.0]
  def change
    create_table :contact_failures do |t|
      t.references :import, null: false, foreign_key: true
      t.integer :row
      t.text :input
      t.text :log

      t.timestamps
    end
  end
end
