class AddImportIdToContacts < ActiveRecord::Migration[6.0]
  def change
    add_reference :contacts, :import, null: false, foreign_key: true
  end
end
